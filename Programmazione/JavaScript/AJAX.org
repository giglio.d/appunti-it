* AJAX
** gestire richieste sincrone
esempio:
#+begin_src js
  var xhr = new XMLHttpRequest();
  xhr.open("GET","script.php", true);
  xhr.send();

  // parsing response
  var text = xhr.responseText;
  var xml  = xhr.responseXML;
#+end_src

** Analizzare un payload
#+begin_src js
  var xhr = new XMLHttpRequest();
  xhr.open("GET","script.php", true);
  xhr.send();

  // parsing response
  var json = JSON.parse(xhr.responseText);

  var target = document.getElementById("main");
  target.innerHTML = json.last_name;
#+end_src

** States and Events

XMLHttpRequest resituisce 5 possibili valori:

0) connessione creata ma non aperta;
1) connessione aperta;
2) richiesta inviata e ricevuda dal server;
3) risposta in corso (dati parziali);
4) risposta completa (successo/fallimento);

#+begin_src js
  var xhr = new XMLHttpRequest();
  xhr.open("GET","script.php", true);

  xhr.onreadystatechange = function() {
      if(xhr.readyState == 4 && xhr.status == 200) {
	  // parsing response
	  var target = document.getElementById("main");
	  target.innerHTML = xhr.responseText;
      }
      xhr.send();
  }
#+end_src
   
#+begin_src js
  function replaceElementById(id, text) {
      document.getelementById(id).innetHTML = text;
  }

  var xhr = new XMLHttpRequest();
    xhr.open("GET","script.php", true);

    xhr.onreadystatechange = function() {
	if(xhr.readyState == 4 && xhr.status == 200) {
	    // parsing response
	    var target = document.getElementById("main");
	    target.innerHTML = xhr.responseText;
	}
	xhr.send();
    }
#+end_src

#+begin_src html
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Ajax Text</title>
    </head>
    <body>

      <div id="main">
	This is the original text when the page first loads.
      </div>

      <button id="ajax-button" type="button">Update content with Ajax</button>

      <script>
	[ ... ]
      </script>

    </body>
  </html>
#+end_src

#+begin_src js
  function replaceText() {
      var xhr = new XMLHttpRequest();
      xhr.open("GET","new_content.txt", true);

      xhr.onreadystatechange = function() {
	  if(xhr.readyState == 2) {
	      target.innerHTML = 'Loading...';
	  }
	  if(xhr.readyState == 4 && xhr.status == 200) {
	      // parsing response
	      // var target = document.getElementById("main");
	      target.innerHTML = xhr.responseText;
	  }
      }
      xhr.send();
  }

  var button = document.getElementById ("ajax-button");
  button.addEventListener("click", replaceText);

#+end_src

** caricare JSON remoti
Esempio:
#+begin_src html
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Ajax Zip Code</title>
      <style>
	#entry {
	margin: 2em 1em;
	}
	#location {
	margin: 1em;
	}
      </style>
    </head>
    <body>

      <div id="entry">
	Zip code: <input id="zipcode" type="text" name="zipcode" />
	<button id="ajax-button" type="button">Submit</button>
      </div>

      <div id="location">
      </div>

      <script>
	[ ... ]
      </script>

    </body>
  </html> 
#+end_src

#+begin_src js
  // Google asks for all API users to sign up for an API key
  // For simple tests, still works without an API key
  var api = 'http://maps.googleapis.com/maps/api/geocode/json';

  function findLocation() {
      var zipcode = documnt.getElementById('zipcode');
      var url = api + '?address=' * zipvalue.value;

      var xhr = new XMLHttpRequest();
      xhr.open("GET","new_content.txt", true);

      xhr.onreadystatechange = function() {
	  if(xhr.readyState < 4) {
	      showLoading();
	  }
	  if(xhr.readyState == 4 && xhr.status == 200) {
	      // parsing response
	      // var target = document.getElementById("main");
	      outputLocation(xhr.responseText);
	  }
      };
      xhr.send();
  }

  function showLoading() {
      var target = document.getEelementById('location');
      target.innerHTML = 'Loading...';
  }

  function outputLocation(data) {
      var target = document.getEelementById('location');
      var json = JSON.parse(data);
      var address = json.result[0].formatted_address;
      target.innerHTML = address;
  }

  var button = document.getElementById ("ajax-button");
  button.addEventListener("click", findLocation);

#+end_src

** jQuery
Codice:
#+begin_src js
  $.ajax({
      type:	"GET",
      url:	"script.php",
      async:	true,
      data:	{ },
      dataType:	"text",
      success:	function(data) {
	  $("#main").html(data);
      },
      errro:	function(jqXHR, textStatus, error) { }
  });
#+end_src

** PHP backend
� possibile verificare se una richiesta � originata da Ajax:
#+begin_src js
  function is_ajax_request() {
	return isset($_SERVER['HTTP_X_REQUEST_WITH']) &&
	    $_SERVER['HTTP_X_REQUEST_WITH'] == 'XMLHttpRequest';
    }

  //  [ ... ]

  if(is_ajax_request()) {
      echo "Ajax request";
  } else {
      echo "non Ajax request";
  }
#+end_src

*** Returning partial HTML

** Ajax button
Realizzazione di un pulsante in UI che invia un'istruzione al backend
e che cambia lo stato dell'applicazione
*** Pt 4.1
[[file:Exercise Files/Chapter_04/04_01/ajax_button/index.php][Index]] [[file:Exercise Files/Chapter_04/04_01/ajax_button/favorite.php][Favorite]]
*** Pt 4.2
#+begin_src js
  function favorite()
  {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', 'favorite.php', true);

      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.onreadystatechange = function () {
	  if(xhr.readyState == 4 && xhr.status == 200) {
	      var result = xhr.responseText;
	      console.log('Result: ' + result);
	  }
      };
      xhr.send();
  }
#+end_src
*** Pt 4.3
[[file:Exercise Files/Chapter_04/04_03/ajax_button/index.php][- Index]]
- [[file:Exercise Files/Chapter_04/04_03/ajax_button/favorite.php][Favorite]]
  
#+begin_src js
  function favorite()
  {
      var parent = this.parentElement; // edit
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'favorite.php', true); // stiamo eseguendo una modifica sul DB

      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.onreadystatechange = function () {
	  if(xhr.readyState == 4 && xhr.status == 200) {
	      var result = xhr.responseText;
	      console.log('Result: ' + result);
	  }
      };
      xhr.send();
  }
#+end_src

#+begin_src perl
  
#+end_src

** Sending Ajax module
*** Pt 5.1
- [[file:Exercise Files/Chapter_05/05_01/ajax_form/index.php][Index]]
- [[file:Exercise Files/Chapter_05/05_01/ajax_form/process_measurements.php][Measurements]]

**** Pt 5.3
#+begin_src perl
    $errors[];
    if($length == '') {$errors[] == 'lenght'};
    if($width == '') {$errors[] == 'lenght'};
    if($height == '') {$errors[] == 'height'};

    if(!empty($errors)) {
	echo "{ 'erros': " . json_encode($errors) . "}";
  }
#+end_src
*** Adding a spinner
- [[file:Exercise Files/Chapter_05/05_04/ajax_form/index.php][Index]]
- [[file:Exercise Files/Chapter_05/05_04/ajax_form/process_measurements.php][Measurements]]
- 
*** Disabling form
*** Allow fallback to HTML
*** Preventing default
- [[file:Exercise Files/Chapter_05/05_07-final/ajax_form/index.php][Index]]
- [[file:Exercise Files/Chapter_05/05_07-final/ajax_form/process_measurements.php][Measurements]]
- 
** Infinite scrolling
*** Appending to HTML
#+begin_src js
function appendToDiv(div, new_hthml) {
    var temp = document.createElement('div');
    temp.innerHTML = new_html;

    var class_name = temp.firstElementChild.className;
    /* il motivo per cui non usiamo firstChild � che considererebbe anche gli
     ,* spazi bianchi. firstElementChild.className esamina il primo figlio e e
     ,* scopre il nome della classe, poi torna indietro e e cattura solo i dre
     ,* div */

    var items = temp.getElementsByClassName(class_name);

}
#+end_src
** Automatic suggestions
